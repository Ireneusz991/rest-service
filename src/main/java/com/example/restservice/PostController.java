package com.example.restservice;

import Dto.Klient;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@RestController
public class PostController {

    @RequestMapping(value = "/test/postmethod/sayhello", method = RequestMethod.POST, consumes = "application/json")
    public String getData(@RequestBody Klient klient) {
        Date date = new Date();
        return "Imie klienta: " + klient.getName() + " Nazwisko klienta: " + klient.getSecondName() + " " + date;
    }

}



  /*
    TODO:
        w requescie powinny być 2 stringi, które nalezy dodać do
        siebie zwrócić do klienta jako jeden oraz
        godzine jako dodatkowe pole
    */